Introducción a Ciencias de la Computación
=========================================

Proyecto 1: Base de datos de Distribuciones de Linux
-------------------------------------------------------

### Fecha de entrega: Viernes 5 de Abril de 2019


Usando la práctica 5 del curso como base deben escribir una aplicación de base de datos equivalente, pero en lugar de Estudiantes deben utilizar algún otro tipo de registro que pueden elegir libremente. En este registro al menos un campo debe ser cadena, al menos uno entero y al menos uno doble. El registro debe tener por lo menos cinco campos.

Si implementan pruebas unitarias suficientemente buenas y no triviales para las clases que ustedes escriban (distintas a las incluidas en las prácticas del curso, aunque pueden basarse en ellas), se pueden ganar un punto extra.

El proyecto debe adaptar todo de la práctica 5, así que la aplicación debe funcionar igual: o bien pidiendo varios registros campo por campo y guardando la base de datos en un archivo especificado por la línea de comandos; o bien cargando la base de datos de un un archivo especificado por la línea de comandos. En ambos casos, el programa debe mostrar los registros en la base de datos y hacer 2 consultas sobre campos diferentes de los registros.

No pueden importar ninguna clase en su proyecto, a menos que las prácticas también la importen. No pueden modificar de ninguna manera las clases incluidas en la práctica 5, incluyendo el nombre de los paquetes.

El proyecto se entregará únicamente al profesor.

Este proyecto 1 consiste en hacer una base de datos sobre distribuciones de linux, en los campos del registro se agregaran un campo extra para cumplir con los 5 campos necesarios, los campos que estaran en el proyecto seran los siguientes:

* `Nombre de la distribucion`,
* `Version de distribucion`,
* `Calificacion del 1 al 10`,
* `Distrubucion derivada` y
* `Gestor de paquetes`.

*No deben modificar de ninguna manera ninguno de los otros archivos de la practica5, la practica sirve de referencia*.

### Repositorio

Pueden clonar el Proyecto 1 con el siguiente comando:

```shell
$ git clone https://gitlab.com/alvaro-ramirez/proyecto-1-de-introduccion-a-ciencias-de-la-computacion.git
```

### Documentación

La documentación generada por JavaDoc la pueden consultar aquí:

Documentación generada por JavaDoc para el Proyecto:
[Pagina Principal](./target/site/index.html)

Reporte general del proyecto (resultados del test,etc):
[Pagina de Reporte](./target/site/project-reports.html)
