package mx.unam.ciencias.icc;

/**
 * Clase para bases de datos de distribuciones de linux.
 */
public class BaseDeDatosDistribucion extends BaseDeDatos {

    /**
     * Crea una distribucion en blanco.
     * @return una distribucion en blanco.
     */
    @Override public Registro creaRegistro() {
        Distribucion d = new Distribucion(null, 0, 0, null, null);
        return d;
    }
}
