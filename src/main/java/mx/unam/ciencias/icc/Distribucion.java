package mx.unam.ciencias.icc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Clase para representar distribuciones. Una distribucion tiene nombre, version de
 * distribucion, calificacion, distribucion derivada de otra distribucion,
 * y gestor de paquetes. La clase implementa {@link Registro}, por lo que puede
 * cargarse y guardarse utilizando objetos de las clases {@link BufferedReader}
 * y {@link BufferedWriter} como entrada y salida respectivamente, además de
 * determinar si sus campos cazan valores arbitrarios.
 */
public class Distribucion implements Registro {

    /* Nombre del distribucion. */
    private String nombre;
    /* Version de la distribucion. */
    private double version;
    /* calificacion.*/
    private int calificacion;
    /* Deriva de otra distribucion.*/
    private String derivaDe;
    /*Gestor de paquetes*/
    private String gestor;

    /**
     * Define el estado inicial de un distribucion.
     * @param nombre el nombre del distribucion.
     * @param version el número de la version de la distribucion.
     * @param calificacion la calificacion otorgada a la distribucion.
     * @param derivaDe de que otra distribucion deriva.
     * @param gestor el nombre del gestor de paquetes.
     */
    public Distribucion(String nombre,
                      double version,
                      int calificacion,
                      String derivaDe,
                      String gestor) {
        this.nombre = nombre;
        this.version = version;
        this.calificacion = calificacion;
        this.derivaDe = derivaDe;
        this.gestor = gestor;
    }

    /**
     * Regresa el nombre del distribucion.
     * @return el nombre del distribucion.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el nombre del distribucion.
     * @param nombre el nuevo nombre del distribucion.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Regresa la version de la distribucion.
     * @return la version de la distribucion.
     */
    public double getVersion() {
        return this.version;
    }

    /**
     * Define la version de la distribucion.
     * @param version define la version de la distribucion.
     */
    public void setVersion(double version) {
        this.version = version;
    }

    /**
     * Regresa la calificacion de la distribucion.
     * @return la calificacion de la distribucion.
     */
    public int getCalificacion() {
        return this.calificacion;
    }

    /**
     * Define la calificacion de la distribucion.
     * @param calificacion la calificacion de la distribucion.
     */
    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    /**
     * Regresa el nombre de la distribucion de la cual deriva.
     * @return el nombre de la distribucion de la cual deriva.
     */
    public String getDerivaDe() {
        return this.derivaDe;
    }

    /**
     * Define el nombre de la distribucion de la que deriva.
     * @param derivaDe la distribucion de la que deriva.
     */
    public void setDerivaDe(String derivaDe) {
        this.derivaDe = derivaDe;
    }

    /**
     * Regresa el nombre del gestor de paquetes que utiliza la distribucion.
     * @return el nombre del gestor de paquetes que utiliza la distribucion.
     */
    public String getGestor() {
        return this.gestor;
    }

    /**
     * Define el nombre del gestor de paquetes de la distribucion.
     * @param gestor el nombre del gestor de paquetes de la distribucion.
     */
    public void setGestor(String gestor) {
        this.gestor = gestor;
    }

    /**
     * Regresa una representación en cadena del distribucion.
     * @return una representación en cadena del distribucion.
     */
    @Override public String toString() {
        String cadena = String.format("Nombre               : %s\n" +
                                      "Version              : %2.2f\n" +
                                      "Calificacion         : %02d\n" +
                                      "Deriva de            : %s\n" +
                                      "Gestor de paquetes   : %s",
                                      nombre, version, calificacion, derivaDe, gestor);
        return cadena;
    }

    /**
     * Nos dice si el objeto recibido es una distribucion igual a la que manda llamar
     * el método.
     * @param objeto el objeto con el que la distribucion se comparará.
     * @return <tt>true</tt> si el objeto recibido es una distribucion con las
     *         mismas propiedades que el objeto que manda llamar al método,
     *         <tt>false</tt> en otro caso.
     */
    @Override public boolean equals(Object objeto) {
        if (!(objeto instanceof Distribucion))
            return false;
        Distribucion distribucion = (Distribucion)objeto;
        return (distribucion == null) 
        ||(this.nombre.equals(distribucion.nombre))
        ||(this.version == (distribucion.version))
        ||(this.calificacion == (distribucion.calificacion))
        ||(this.derivaDe.equals(distribucion.derivaDe))
        ||(this.gestor.equals(distribucion.gestor)) ? true : false;
    }

    /**
     * Guarda la distribucion en la salida recibida.
     * @param out la salida dónde hay que guardar la distribucion.
     * @throws IOException si un error de entrada/salida ocurre.
     */
    @Override public void guarda(BufferedWriter out) throws IOException {
        out.write(String.format("%s\t%2.2f\t%02d\t%s\t%s\n",
                                nombre,
                                version,
                                calificacion,
                                derivaDe,
                                gestor));
    }

    /**
     * Carga la distribucion de la entrada recibida.
     * @param in la entrada de dónde hay que cargar la distribucion.
     * @return <tt>true</tt> si el método carga una distribucion valida,
     *         <tt>false</tt> en otro caso.
     * @throws IOException si un error de entrada/salida ocurre, o si la entrada
     *         recibida no contiene a una distribucion.
     */
    @Override public boolean carga(BufferedReader in) throws IOException {
        String l=in.readLine();
        if (l == null)
           return false;
        l=l.trim();
        if(l.equals(""))
            return false;
        String[]t=l.split("\t");
        if(t.length != 5)
          throw new IOException();
        nombre = t[0];
        try{
          version = Double.parseDouble(t[1]);
          calificacion = Integer.parseInt(t[2]);
          derivaDe = t[3];
          gestor = t[4];

        } catch(NumberFormatException nfe){
            throw new IOException("error entrada salida");
          }
        return true;
    }

    /**
     * Nos dice si la distribucion caza el valor dado en el campo especificado.
     * @param campo el campo que hay que cazar.
     * @param valor el valor con el que debe cazar el campo del registro.
     * @return <tt>true</tt> si:
     *         <ul>
     *           <li><tt>campo</tt> es {@link CampoDistribucion#NOMBRE} y
     *              <tt>valor</tt> es instancia de {@link String} y es una
     *              subcadena del nombre de la distribucion.</li>
     *           <li><tt>campo</tt> es {@link CampoDistribucion#VERSION} y
     *              <tt>valor</tt> es instancia de {@link Double} y su
     *              valor entero es mayor o igual a la version de la
     *              distribucion.</li>
     *           <li><tt>campo</tt> es {@link CampoDistribucion#CALIFICACION} y
     *              <tt>valor</tt> es instancia de {@link Integer} y su
     *              valor doble es mayor o igual a la calificacion de la
     *              distribucion.</li>
     *           <li><tt>campo</tt> es {@link CampoDistribucion#DERIVA} y
     *              <tt>valor</tt> es instancia de {@link String} y es una
     *              subcadena del nombre de la distibucion derivada.</li>
     *           <li><tt>campo</tt> es {@link CampoDistribucion#GESTOR} y
     *              <tt>valor</tt> es instancia de {@link String} y es una
     *              subcadena del nombre del gestor.</li>
     *         </ul>
     *         <tt>false</tt> en otro caso.
     * @throws IllegalArgumentException si el campo no es instancia de
     *         {@link CampoDistribucion}.
     */
    public boolean caza(Enum campo, Object valor) {
        if (!(campo instanceof CampoDistribucion))
            throw new IllegalArgumentException("El campo debe ser " +
                                               "CampoDistribucion");
        CampoDistribucion cd = (CampoDistribucion)campo;
        switch(cd){
          case NOMBRE: return cazaNombre(valor);
          case VERSION: return cazaVersion(valor);
          case CALIFICACION: return cazaCalificacion(valor);
          case DERIVA: return cazaDeriva(valor);
          case GESTOR: return cazaGestor(valor);
          default: return false;
        }
    }

    private boolean cazaNombre(Object o){
      if(!(o instanceof String))
        return false;
      String v=(String)o;
      if(v.isEmpty())
        return false;
      return nombre.indexOf(v)!=-1;
    }

    private boolean cazaVersion(Object o){
      if(!(o instanceof Double))
        return false;
        Double v = (Double)o;
        return version >= v.doubleValue();
    }

    private boolean cazaCalificacion(Object o){
      if(!(o instanceof Integer))
        return false;
      Integer v = (Integer)o;
      return calificacion >= v.intValue();
    }

    private boolean cazaDeriva(Object o){
      if(!(o instanceof String))
        return false;
      String v = (String)o;
      if(v.isEmpty())
        return false;
      return derivaDe.indexOf(v)!=-1;
    }

    private boolean cazaGestor(Object o){
      if(!(o instanceof String))
        return false;
      String v = (String)o;
      if(v.isEmpty())
        return false;
      return gestor.indexOf(v)!=-1;
    }
}
