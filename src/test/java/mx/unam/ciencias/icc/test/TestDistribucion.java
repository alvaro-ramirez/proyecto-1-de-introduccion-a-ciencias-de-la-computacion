package mx.unam.ciencias.icc.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Random;
import mx.unam.ciencias.icc.CampoDistribucion;
import mx.unam.ciencias.icc.Distribucion;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

/**
 * Clase para pruebas unitarias de la clase {@link Distribucion}.
 */
public class TestDistribucion {

    /** Expiración para que ninguna prueba tarde más de 5 segundos. */
    @Rule public Timeout expiracion = Timeout.seconds(5);

    /* Nombres. */
    private static final String[] NOMBRES = {
        "Ubuntu", "Fedora", "Debian", "Gentoo", "Linux Mint",
        "Red Hat", "Antergos", "Manjaro", "Arch Linux", "OpenSUSE"
    };

    /* DERIVA. */
    private static final String[] DERIVA = {
        "Arch Linux", "Debian", "Red Hat", "Ninguna"
    };

    /* GESTOR. */
    private static final String[] GESTOR = {
        "Yum", "Apt", "Pacman"
    };

    /* Generador de números aleatorios. */
    private static Random random;

    /**
     * Genera un nombre aleatorio.
     * @return un nombre aleatorio.
     */
    public static String nombreAleatorio() {
        int n = random.nextInt(NOMBRES.length);
        return NOMBRES[n];
    }

    /**
     * Genera un número de version aleatorio.
     * @return un número de version aleatorio.
     */
    public static double versionAleatorio() {
        /* Estúpida precisión. Eso dice Canek xD :V */
        return random.nextInt(100) / 10.0;
        
    }

    /**
     * Genera una calificacion aleatoria.
     * @return una calificacion aleatoria.
     */
    public static int calificacionAleatoria() {
        return 1000000 + random.nextInt(1000000);
    }

    /**
     * Genera un nombre aleatorio.
     * @return un nombre aleatorio.
     */
    public static String derivaDeAleatorio() {
        int n = random.nextInt(DERIVA.length);
        return DERIVA[n];
    }

    /**
     * Genera un nombre aleatorio.
     * @return un nombre aleatorio.
     */
    public static String gestorAleatorio() {
        int n = random.nextInt(GESTOR.length);
        return GESTOR[n];
    }

    /**
     * Genera un distribucion aleatorio.
     * @return un distribucion aleatorio.
     */
    public static Distribucion distribucionAleatorio() {
        return new Distribucion(nombreAleatorio(),
                              versionAleatorio(),
                              calificacionAleatoria(),
                              derivaDeAleatorio(),
                              gestorAleatorio());
    }

    /**
     * Genera un distribucion aleatorio con un número de version dado.
     * @param version el número de version de la distribucion.
     * @return un distribucion aleatorio.
     */
    public static Distribucion distribucionAleatorio(double version) {
        return new Distribucion(nombreAleatorio(),
                              version,
                              calificacionAleatoria(),
                              derivaDeAleatorio(),
                              gestorAleatorio());
    }

    /* La distribucion. */
    private Distribucion distribucion;

    /* Enumeración espuria. */
    private enum X {
        /* Campo espurio. */
        A;
    }

    /**
     * Prueba unitaria para {@link
     * Distribucion#Distribucion(String,double,int,String,String)}.
     */
    @Test public void testConstructor() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.getNombre().equals(nombre));
        Assert.assertTrue(distribucion.getVersion() == version);
        Assert.assertTrue(distribucion.getCalificacion() == calificacion);
        Assert.assertTrue(distribucion.getDerivaDe().equals(derivaDe));
        Assert.assertTrue(distribucion.getGestor().equals(gestor));


    }

    /**
     * Prueba unitaria para {@link Distribucion#getNombre}.
     */
    @Test public void testGetNombre() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.getNombre().equals(nombre));
        Assert.assertFalse(distribucion.getNombre().equals(nombre + " X"));
    }

    /**
     * Prueba unitaria para {@link Distribucion#setNombre}.
     */
    @Test public void testSetNombre() {
        String nombre = nombreAleatorio();
        String nuevoNombre = nombre + " X";
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.getNombre().equals(nombre));
        Assert.assertFalse(distribucion.getNombre().equals(nuevoNombre));
        distribucion.setNombre(nuevoNombre);
        Assert.assertFalse(distribucion.getNombre().equals(nombre));
        Assert.assertTrue(distribucion.getNombre().equals(nuevoNombre));
    }

    /**
     * Prueba unitaria para {@link Distribucion#getVersion}.
     */
    @Test public void testGetVersion() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.getVersion() == version);
        Assert.assertFalse(distribucion.getVersion() == version + 1.1500);
    }

    /**
     * Prueba unitaria para {@link Distribucion#setVersion}.
     */
    @Test public void testSetVersion() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        double nuevaVersion = version + 1.1500;
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.getVersion() == version);
        Assert.assertFalse(distribucion.getVersion() == version + 1.1500);
        distribucion.setVersion(nuevaVersion);
        Assert.assertFalse(distribucion.getVersion() == version);
        Assert.assertTrue(distribucion.getVersion() == nuevaVersion);
    }

    /**
     * Prueba unitaria para {@link Distribucion#getCalificacion}.
     */
    @Test public void testGetCalificacion() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.getCalificacion() == calificacion);
        Assert.assertFalse(distribucion.getCalificacion() == calificacion + 1);
    }

    /**
     * Prueba unitaria para {@link Distribucion#setCalificacion}.
     */
    @Test public void testSetCalificacion() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        int nuevaCalificacion = calificacion + 1;
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.getCalificacion() == calificacion);
        Assert.assertFalse(distribucion.getCalificacion() == nuevaCalificacion);
        distribucion.setCalificacion(nuevaCalificacion);
        Assert.assertFalse(distribucion.getCalificacion() == calificacion);
        Assert.assertTrue(distribucion.getCalificacion() == nuevaCalificacion);
    }

    /**
     * Prueba unitaria para {@link Distribucion#getDerivaDe}.
     */
    @Test public void testGetDerivaDe() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.getDerivaDe().equals(derivaDe));
        Assert.assertFalse(distribucion.getDerivaDe().equals(derivaDe + "X"));
    }

    /**
     * Prueba unitaria para {@link Distribucion#setDerivaDe}.
     */
    @Test public void testSetDerivaDe() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        String nuevoderivaDe = derivaDe + "50";
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.getDerivaDe().equals(derivaDe));
        Assert.assertFalse(distribucion.getDerivaDe().equals(nuevoderivaDe));
        distribucion.setDerivaDe(nuevoderivaDe);
        Assert.assertFalse(distribucion.getDerivaDe().equals(derivaDe));
        Assert.assertTrue(distribucion.getDerivaDe().equals(nuevoderivaDe));
    }

    /**
     * Prueba unitaria para {@link Distribucion#getGestor}
     */
    @Test public void testGetGestor(){
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.getGestor().equals(gestor));
        Assert.assertFalse(distribucion.getGestor().equals(gestor + "X"));
    }

    /**
     * Prueba unitaria para {@link Distribucion#setGestor}
     */
    @Test public void testSetGestor(){
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        String nuevoGestor = gestor + "x";
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.getGestor().equals(gestor));
        Assert.assertFalse(distribucion.getGestor().equals(nuevoGestor));
        distribucion.setGestor(nuevoGestor);
        Assert.assertFalse(distribucion.getGestor().equals(gestor));
        Assert.assertTrue(distribucion.getGestor().equals(nuevoGestor));
    }

    /**
     * Prueba unitaria para {@link Distribucion#toString}.
     */
    @Test public void testToString() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        String cadena = String.format("Nombre               : %s\n" +
                                      "Version              : %2.2f\n" +
                                      "Calificacion         : %02d\n" +
                                      "Deriva de            : %s\n" +
                                      "Gestor de paquetes   : %s",
                                      nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.toString().equals(cadena));
        version = 18.05;
        calificacion = 10;
        distribucion.setVersion(version);
        distribucion.setCalificacion(calificacion);
        cadena = String.format("Nombre               : %s\n" +
                               "Version              : %2.2f\n" +
                               "Calificacion         : %02d\n" +
                               "Deriva de            : %s\n" +
                               "Gestor de paquetes   : %s",
                                nombre, version, calificacion, derivaDe, gestor);
        Assert.assertTrue(distribucion.toString().equals(cadena));
    }

    /**
     * Prueba unitaria para {@link Distribucion#equals}.
     */
    @Test public void testEquals() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        Distribucion distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);
        String otroNombre = nombre+"Otra cosa";
        double otraVersion = version+2.2;
        int otraCalificacion = calificacion+2;
        String otraderivaDe = derivaDe+"otra cosa";
        String otroGestor = gestor+"otra cosa";
        Distribucion distinto = new Distribucion(otroNombre, otraVersion, otraCalificacion, otraderivaDe, otroGestor);
        Assert.assertFalse(distribucion.equals(distinto));
        Assert.assertFalse(distribucion.equals(distinto));
        Assert.assertFalse(distribucion.equals("Una cadena"));
        Assert.assertFalse(distribucion.equals(null));
    }

    /**
     * Prueba unitaria para {@link Distribucion#guarda}.
     */
    @Test public void testGuarda() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);

        try {
            StringWriter swOut = new StringWriter();
            BufferedWriter out = new BufferedWriter(swOut);
            distribucion.guarda(out);
            out.close();
            String guardado = swOut.toString();
            String s = String.format("%s\t%2.2f\t%02d\t%s\t%s\n",
                                    nombre,
                                    version,
                                    calificacion,
                                    derivaDe,
                                    gestor);
                Assert.assertTrue(guardado.equals(s));
        } catch (IOException ioe) {
            Assert.fail();
        }
    }

    /**
     * Prueba unitaria para {@link Distribucion#carga}.
     */
    @Test public void testCarga() {
        distribucion = new Distribucion(null, 0.0, 0, null, null);

        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();

        String entrada = String.format("%s\t%2.2f\t%02d\t%s\t%s\n",
                                        nombre,
                                        version,
                                        calificacion,
                                        derivaDe,
                                        gestor);

        try {
            StringReader srIn = new StringReader(entrada);
            BufferedReader in = new BufferedReader(srIn);
            Assert.assertTrue(distribucion.carga(in));
            in.close();
            Assert.assertTrue(distribucion.getNombre().equals(nombre));
            Assert.assertTrue(distribucion.getVersion() == version);
            Assert.assertTrue(distribucion.getCalificacion() == calificacion);
            Assert.assertTrue(distribucion.getDerivaDe().equals(derivaDe));
            Assert.assertTrue(distribucion.getGestor().equals(gestor));
        } catch (IOException ioe) {
            Assert.fail();
        }

        entrada = "";
        try {
            StringReader srIn = new StringReader(entrada);
            BufferedReader in = new BufferedReader(srIn);
            Assert.assertFalse(distribucion.carga(in));
            in.close();
            Assert.assertTrue(distribucion.getNombre().equals(nombre));
            Assert.assertTrue(distribucion.getVersion() == version);
            Assert.assertTrue(distribucion.getCalificacion() == calificacion);
            Assert.assertTrue(distribucion.getDerivaDe().equals(derivaDe));
            Assert.assertTrue(distribucion.getGestor().equals(gestor));
        } catch (IOException ioe) {
            Assert.fail();
        }

        entrada = "\n";
        try {
            StringReader srIn = new StringReader(entrada);
            BufferedReader in = new BufferedReader(srIn);
            Assert.assertFalse(distribucion.carga(in));
            in.close();
            Assert.assertTrue(distribucion.getNombre().equals(nombre));
            Assert.assertTrue(distribucion.getVersion() == version);
            Assert.assertTrue(distribucion.getCalificacion() == calificacion);
            Assert.assertTrue(distribucion.getDerivaDe().equals(derivaDe));
            Assert.assertTrue(distribucion.getGestor().equals(gestor));
        } catch (IOException ioe) {
            Assert.fail();
        }
    }

    /**
     * Prueba unitaria para {@link Distribucion#caza}.
     */
    @Test public void testCaza() {
        String nombre = nombreAleatorio();
        double version = versionAleatorio();
        int calificacion = calificacionAleatoria();
        String derivaDe = derivaDeAleatorio();
        String gestor = gestorAleatorio();
        distribucion = new Distribucion(nombre, version, calificacion, derivaDe, gestor);

        String n = distribucion.getNombre();
        int m = distribucion.getNombre().length();
        Assert.assertTrue(distribucion.caza(CampoDistribucion.NOMBRE, n));
        n = distribucion.getNombre().substring(0, m/2);
        Assert.assertTrue(distribucion.caza(CampoDistribucion.NOMBRE, n));
        n = distribucion.getNombre().substring(m/2, m);
        Assert.assertTrue(distribucion.caza(CampoDistribucion.NOMBRE, n));
        n = distribucion.getNombre().substring(m/3, 2*(m/3));
        Assert.assertTrue(distribucion.caza(CampoDistribucion.NOMBRE, n));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.NOMBRE, ""));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.NOMBRE, "XXX"));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.NOMBRE,
                                           new Integer(8)));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.NOMBRE, null));

        Double v = new Double(distribucion.getVersion());
        Assert.assertTrue(distribucion.caza(CampoDistribucion.VERSION, v));
        v = new Double(distribucion.getVersion() - 18.50);
        Assert.assertTrue(distribucion.caza(CampoDistribucion.VERSION, v));
        v = new Double(distribucion.getVersion() + 18.50);
        Assert.assertFalse(distribucion.caza(CampoDistribucion.VERSION, v));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.VERSION, "XXX"));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.VERSION, null));

        Integer c = new Integer(distribucion.getCalificacion());
        Assert.assertTrue(distribucion.caza(CampoDistribucion.CALIFICACION, c));
        c = new Integer(distribucion.getCalificacion() - 5);
        Assert.assertTrue(distribucion.caza(CampoDistribucion.CALIFICACION, c));
        c = new Integer(distribucion.getCalificacion() + 5);
        Assert.assertFalse(distribucion.caza(CampoDistribucion.CALIFICACION, c));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.CALIFICACION, "XXX"));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.CALIFICACION, null));

        String d = distribucion.getDerivaDe();
        int e = distribucion.getDerivaDe().length();
        Assert.assertTrue(distribucion.caza(CampoDistribucion.DERIVA, d));
        d = distribucion.getDerivaDe().substring(0, e/2);
        Assert.assertTrue(distribucion.caza(CampoDistribucion.DERIVA, d));
        d = distribucion.getDerivaDe().substring(e/2, e);
        Assert.assertTrue(distribucion.caza(CampoDistribucion.DERIVA, d));
        d = distribucion.getDerivaDe().substring(e/3, 2*(e/3));
        Assert.assertTrue(distribucion.caza(CampoDistribucion.DERIVA, d));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.DERIVA, ""));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.DERIVA, "XXX"));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.DERIVA,
                                           new Integer(1000)));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.DERIVA, null));

        String g = distribucion.getGestor();
        int s = distribucion.getGestor().length();
        Assert.assertTrue(distribucion.caza(CampoDistribucion.GESTOR, g));
        g = distribucion.getGestor().substring(0, s/2);
        Assert.assertTrue(distribucion.caza(CampoDistribucion.GESTOR, g));
        g = distribucion.getGestor().substring(s/2, s);
        Assert.assertTrue(distribucion.caza(CampoDistribucion.GESTOR, g));
        g = distribucion.getGestor().substring(s/3, 2*(s/3));
        Assert.assertTrue(distribucion.caza(CampoDistribucion.GESTOR, g));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.GESTOR, ""));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.GESTOR, "XXX"));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.GESTOR,
                                           new Integer(1000)));
        Assert.assertFalse(distribucion.caza(CampoDistribucion.GESTOR, null));

        try {
            distribucion.caza(null, null);
            Assert.fail();
        } catch (IllegalArgumentException iae) {}
        try {
            distribucion.caza(X.A, distribucion.getNombre());
            Assert.fail();
        } catch (IllegalArgumentException iae) {}
        try {
            Object o = new Double(distribucion.getVersion());
            distribucion.caza(X.A, o);
        } catch (IllegalArgumentException iae) {}
        try {
            Object o = new Integer(distribucion.getCalificacion());
            distribucion.caza(X.A, o);
        } catch (IllegalArgumentException iae) {}
        try {
            Object o = new String(distribucion.getDerivaDe());
            distribucion.caza(X.A, o);
        } catch (IllegalArgumentException iae) {}
        try {
            Object o = new String(distribucion.getGestor());
            distribucion.caza(X.A, o);
        } catch (IllegalArgumentException iae) {}
        try {
            Assert.assertFalse(distribucion.caza(X.A, null));
        } catch (IllegalArgumentException iae) {}
    }

    /* Inicializa el generador de números aleatorios. */
    static {
        random = new Random();
    }
}
