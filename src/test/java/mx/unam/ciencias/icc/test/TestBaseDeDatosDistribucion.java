package mx.unam.ciencias.icc.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Random;
import mx.unam.ciencias.icc.BaseDeDatos;
import mx.unam.ciencias.icc.BaseDeDatosDistribucion;
import mx.unam.ciencias.icc.CampoDistribucion;
import mx.unam.ciencias.icc.Distribucion;
import mx.unam.ciencias.icc.Lista;
import mx.unam.ciencias.icc.test.TestDistribucion;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

/**
 * Clase para pruebas unitarias de la clase {@link BaseDeDatosDistribucion}.
 */
public class TestBaseDeDatosDistribucion {

    /** Expiración para que ninguna prueba tarde más de 5 segundos. */
    @Rule public Timeout expiracion = Timeout.seconds(5);

    /* Generador de números aleatorios. */
    private Random random;
    /* Base de datos de distribuciones. */
    private BaseDeDatosDistribucion bdd;
    /* Número total de distribuciones. */
    private int total;

    /* Enumeración espuria. */
    private enum X {
        /* Campo espurio. */
        A;
    }

    /**
     * Crea un generador de números aleatorios para cada prueba y una base de
     * datos de distribuciones.
     */
    public TestBaseDeDatosDistribucion() {
        random = new Random();
        bdd = new BaseDeDatosDistribucion();
        total = 1 + random.nextInt(100);
    }

    /**
     * Prueba unitaria para {@link
     * BaseDeDatosDistribucion#BaseDeDatosDistribucion}.
     */
    @Test public void testConstructor() {
        Lista distribuciones = bdd.getRegistros();
        Assert.assertFalse(distribuciones == null);
        Assert.assertTrue(distribuciones.getLongitud() == 0);
        Assert.assertTrue(bdd.getNumRegistros() == 0);
    }

    /**
     * Prueba unitaria para {@link BaseDeDatos#getNumRegistros}.
     */
    @Test public void testGetNumRegistros() {
        Assert.assertTrue(bdd.getNumRegistros() == 0);
        for (int i = 0; i < total; i++) {
            Distribucion e = TestDistribucion.distribucionAleatorio();
            bdd.agregaRegistro(e);
            Assert.assertTrue(bdd.getNumRegistros() == i+1);
        }
        Assert.assertTrue(bdd.getNumRegistros() == total);
    }

    /**
     * Prueba unitaria para {@link BaseDeDatos#getRegistros}.
     */
    @Test public void testGetRegistros() {
        Lista l = bdd.getRegistros();
        Lista r = bdd.getRegistros();
        Assert.assertTrue(l.equals(r));
        Assert.assertFalse(l == r);
        Distribucion[] distribuciones = new Distribucion[total];
        for (int i = 0; i < total; i++) {
            distribuciones[i] = TestDistribucion.distribucionAleatorio();
            bdd.agregaRegistro(distribuciones[i]);
        }
        l = bdd.getRegistros();
        int c = 0;
        Lista.Nodo nodo = l.getCabeza();
        while (nodo != null) {
            Assert.assertTrue(distribuciones[c++].equals(nodo.get()));
            nodo = nodo.getSiguiente();
        }
        l.elimina(distribuciones[0]);
        Assert.assertFalse(l.equals(bdd.getRegistros()));
        Assert.assertFalse(l.getLongitud() == bdd.getNumRegistros());
    }

    /**
     * Prueba unitaria para {@link BaseDeDatos#agregaRegistro}.
     */
    @Test public void testAgregaRegistro() {
            Distribucion d = TestDistribucion.distribucionAleatorio();
            Assert.assertFalse(bdd.getRegistros().contiene(d));
            bdd.agregaRegistro(d);
            Assert.assertTrue(bdd.getRegistros().contiene(d));
            Lista l = bdd.getRegistros();
            Assert.assertTrue(l.get(l.getLongitud() - 1).equals(d));
        
    }

    /**
     * Prueba unitaria para {@link BaseDeDatos#eliminaRegistro}.
     */
    @Test public void testEliminaRegistro() {
            Distribucion d = TestDistribucion.distribucionAleatorio();
            bdd.agregaRegistro(d);
            Assert.assertTrue(bdd.getRegistros().contiene(d));
            bdd.eliminaRegistro(d);
            Assert.assertFalse(bdd.getRegistros().contiene(d));
    }

    /**
     * Prueba unitaria para {@link BaseDeDatos#limpia}.
     */
    @Test public void testLimpia() {
        for (int i = 0; i < total; i++) {
            Distribucion d = TestDistribucion.distribucionAleatorio();
            bdd.agregaRegistro(d);
        }
        Lista registros = bdd.getRegistros();
        Assert.assertFalse(registros.esVacia());
        Assert.assertFalse(registros.getLongitud() == 0);
        bdd.limpia();
        registros = bdd.getRegistros();
        Assert.assertTrue(registros.esVacia());
        Assert.assertTrue(registros.getLongitud() == 0);
    }
    //revisar con ricardo, problema solucionado
    /**
     * Prueba unitaria para {@link BaseDeDatos#guarda}.
     */
    @Test public void testGuarda() {
        Distribucion e = TestDistribucion.distribucionAleatorio();
        bdd.agregaRegistro(e);
        String guardado = "";
        try {
            StringWriter swOut = new StringWriter();
            BufferedWriter out = new BufferedWriter(swOut);
            bdd.guarda(out);
            out.close();
            guardado = swOut.toString();
        } catch (IOException ioe) {
            Assert.fail();
        } 
        // String[] lineas = guardado.split("\n");
        Lista l = bdd.getRegistros();
        Lista.Nodo nodo = l.getCabeza();
        Distribucion d = (Distribucion)nodo.get();
        Assert.assertTrue(e.equals(d));
        String el = String.format("%s\t%2.2f\t%02d\t%s\t%s\n",
                                d.getNombre(),
                                d.getVersion(),
                                d.getCalificacion(),
                                d.getDerivaDe(),
                                d.getGestor());
        Assert.assertTrue(guardado.equals(el));
        

       
    }

    /**
     * Prueba unitaria para {@link BaseDeDatos#carga}.
     */
    @Test public void testCarga() {
        int ini = random.nextInt(1000000);
        String entrada = "";
        Distribucion[] distribuciones = new Distribucion[total];
        for (int i = 0; i < total; i++) {
            distribuciones[i] = TestDistribucion.distribucionAleatorio(ini + i);
            entrada += String.format("%s\t%2.2f\t%02d\t%s\t%s\n",
                                     distribuciones[i].getNombre(),
                                     distribuciones[i].getVersion(),
                                     distribuciones[i].getCalificacion(),
                                     distribuciones[i].getDerivaDe(),
                                     distribuciones[i].getGestor());
            bdd.agregaRegistro(distribuciones[i]);
        }
        try {
            StringReader srInt = new StringReader(entrada);
            BufferedReader in = new BufferedReader(srInt, 8192);
            bdd.carga(in);
            in.close();
        } catch (IOException ioe) {
            Assert.fail();
        }
        Assert.assertTrue(bdd.getNumRegistros() == total);
        int c = 0;
        Lista l = bdd.getRegistros();
        Lista.Nodo nodo = l.getCabeza();
        while (nodo != null) {
            Assert.assertTrue(distribuciones[c++].equals(nodo.get()));
            nodo = nodo.getSiguiente();
        }
        entrada = "";
        try {
            StringReader srInt = new StringReader(entrada);
            BufferedReader in = new BufferedReader(srInt, 8192);
            bdd.carga(in);
            in.close();
        } catch (IOException ioe) {
            Assert.fail();
        }
        Assert.assertTrue(bdd.getNumRegistros() == 0);
    }

    /**
     * Prueba unitaria para {@link BaseDeDatosDistribucion#creaRegistro}.
     */
    @Test public void testCreaRegistro() {
        Distribucion d = (Distribucion)bdd.creaRegistro();
        Assert.assertTrue(d.getNombre() == null);
        Assert.assertTrue(d.getVersion() == 0.0);
        Assert.assertTrue(d.getCalificacion() == 0);
        Assert.assertTrue(d.getDerivaDe() == null);
        Assert.assertTrue(d.getGestor() == null);
    }

    /**
     * Prueba unitaria para {@link BaseDeDatosDistribucion#buscaRegistros}.
     */
    @Test public void testBuscaRegistros() {
        BaseDeDatosDistribucion nueva = new BaseDeDatosDistribucion();
        Distribucion e = TestDistribucion.distribucionAleatorio();
        nueva.agregaRegistro(e);

        Lista l = nueva.getRegistros();
        String nombre = e.getNombre();
        l = nueva.buscaRegistros(CampoDistribucion.NOMBRE,nombre);
        Assert.assertTrue(l.getLongitud() > 0);
        Assert.assertTrue(l.contiene(e));

        Double version = e.getVersion();
        l = nueva.buscaRegistros(CampoDistribucion.VERSION, version);
        Assert.assertTrue(l.getLongitud() > 0);
        Assert.assertTrue(l.contiene(e));

        Integer calificacion = e.getCalificacion();
        l = nueva.buscaRegistros(CampoDistribucion.CALIFICACION, calificacion);
        Assert.assertTrue(l.getLongitud() > 0);
        Assert.assertTrue(l.contiene(e));

        String derivaDe = e.getDerivaDe();
        l = nueva.buscaRegistros(CampoDistribucion.DERIVA,derivaDe);
        Assert.assertTrue(l.getLongitud() > 0);
        Assert.assertTrue(l.contiene(e));

        String gestor = e.getGestor();
        l = nueva.buscaRegistros(CampoDistribucion.GESTOR,gestor);
        Assert.assertTrue(l.getLongitud() > 0);
        Assert.assertTrue(l.contiene(e));

        l = bdd.buscaRegistros(CampoDistribucion.NOMBRE,
                               "xxx-nombre");
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.VERSION,
                               new Double(19.14));
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.CALIFICACION,
                               new Integer(80));
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.DERIVA,
                               "xxx-Ninguna");
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.GESTOR,
                               "xxx-Por defecto");
        Assert.assertTrue(l.esVacia());
        
        l = bdd.buscaRegistros(CampoDistribucion.NOMBRE, "");
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.VERSION,
                               new Integer(Integer.MAX_VALUE));
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.CALIFICACION,
                               new Double(Double.MAX_VALUE));
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.DERIVA, "");
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.GESTOR, "");
        Assert.assertTrue(l.esVacia());

        l = bdd.buscaRegistros(CampoDistribucion.NOMBRE, null);
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.VERSION, null);
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.CALIFICACION, null);
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.DERIVA, null);
        Assert.assertTrue(l.esVacia());
        l = bdd.buscaRegistros(CampoDistribucion.GESTOR, null);
        Assert.assertTrue(l.esVacia());

        try {
            l = bdd.buscaRegistros(null, null);
            Assert.fail();
        } catch (IllegalArgumentException iae) {}
        try {
            l = bdd.buscaRegistros(X.A, null);
            Assert.fail();
        } catch (IllegalArgumentException iae) {}
    }
}
