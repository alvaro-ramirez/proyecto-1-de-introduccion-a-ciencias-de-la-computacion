package mx.unam.ciencias.icc.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Random;
import mx.unam.ciencias.icc.CampoDistribucion;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

/**
 * Clase para pruebas unitarias de la enumeración {@link CampoDistribucion}.
 */
public class TestCampoDistribucion {

    /** Expiración para que ninguna prueba tarde más de 5 segundos. */
    @Rule public Timeout expiracion = Timeout.seconds(5);

    /**
     * Prueba unitaria para {@link CampoDistribucion#toString}.
     */
    @Test public void testToString() {
        String s = CampoDistribucion.NOMBRE.toString();
        Assert.assertTrue(s.equals("Nombre"));
        s = CampoDistribucion.VERSION.toString();
        Assert.assertTrue(s.equals("# De version"));
        s = CampoDistribucion.CALIFICACION.toString();
        Assert.assertTrue(s.equals("Calificacion"));
        s = CampoDistribucion.DERIVA.toString();
        Assert.assertTrue(s.equals("Deriva de"));
        s = CampoDistribucion.GESTOR.toString();
        Assert.assertTrue(s.equals("Gestor"));
    }
}
